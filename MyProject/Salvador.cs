﻿using Robocode;
using System.Drawing;
using Robocode.Util;
using FNL.Geometries;
using System;


namespace FNL
{
    public class Salvador : AdvancedRobot
    {

     
        public override void Run()
        {
            BodyColor = (Color.CadetBlue);
            GunColor = (Color.Black);
            RadarColor = (Color.Orange);
            BulletColor = (Color.Cyan);
            ScanColor = (Color.Cyan);

            // Infinite loop making sure this robot runs till the end of the battle round
            while (true)
            {
                TurnGunRight(360);
                Ahead(0.25 * BattleFieldWidth);
                TurnRight(45);
                Back(0.20 * BattleFieldWidth);
                TurnRight(45);
            }
        }

       
        public override void OnHitByBullet(HitByBulletEvent e) {
            double speed = Rules.GetBulletSpeed(e.Bullet.Power);
            Points enemyLocation = new Points(e.Bullet.X, e.Bullet.Y);
            var distanceToEnemy = Geometry.GetDistance(X, Y, enemyLocation.X, enemyLocation.Y);

            TurnRight(30);

            if(distanceToEnemy > BattleFieldHeight / 1.5)
            {
                TurnRight(20);
                Back(0.15 * BattleFieldHeight);
                return;
            }
        }
        
           
       
        
        public override void OnHitWall(HitWallEvent e)
        {
            TurnRight(90);
            Ahead(0.30 * BattleFieldWidth);
        }

       
        public override void OnScannedRobot(ScannedRobotEvent e)
        {
            if (e.Name.Contains("FNL.MyRobot") )
                return;

            Shoot(e);
        }
        
        public  void  Shoot(ScannedRobotEvent e)
        {

            

            double FirePower = DecideFirePower(e);

                Rules.GetBulletSpeed(FirePower);

            double absolutBearing = e.BearingRadians + HeadingRadians;
            double gunTurn = absolutBearing - GunHeadingRadians;
            double future = e.Velocity * Math.Sin(e.HeadingRadians - absolutBearing) / Rules.GetBulletSpeed(1);
            TurnGunRightRadians(Utils.NormalRelativeAngle(gunTurn + future));
            Fire(FirePower);
        }

        public double DecideFirePower(ScannedRobotEvent e)
        {
            double FirePower = Others == 1 ? 2.0 : 3.0;
            if(e.Distance > 600 ) {
                FirePower = 1.0;
            }
            else if (e.Distance > 400)
            {
                FirePower = 1.5;
            }
            else if(e.Distance < 200 ) {
                FirePower = 3.0;
            }
            if(Energy < 1 ) {
                FirePower = 0.1;
            }
            else if(Energy < 10 ) {
                FirePower = 1.0;
            }
            return Math.Min(e.Energy / 4, FirePower);
        }
    }
}


