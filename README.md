# ROBOCODE - SOLUTIS TALENT SPRINT

## O robô possui quatro métodos principais

### Run - Executa enquanto o robô estiver vivo

* Aparência do robô.

* Movimentação contínua do robô.

### OnHitByBullet - Chamado quando é atingido por um disparo

* Calcula a distância entre meu robô e o robô inimigo.

* Após obter a distância, decido se vou apenas girar o corpo do robô ou se vou girar o corpo e recuo.

### OnHitWall - Chamado quando colide com a parede

* Gira o corpo do robô e recua.

### OnScannedRobot - Quando o radar localiza outro robô

* Verifica se é o meu robô já que é possível colocar robôs iguais, se for verdadeiro ele não irá disparar, caso contrário abrirá fogo.

* Chama o método "Shoot" passando um parâmetro.

* O método "Shoot" chama o método "DecideFirePower" passando um parâmetro que retorna o poder do disparo.

* Com os dados do inimigo bearing,velocidade, direção e sentido é possível projetar a posição do inimigo em um dado tempo futuro, assumindo que os valores não variem.

* O disparo é realizado depois que a arma é posicionada.

## Ponto positivo

* Movimentos curtos e rápidos para não ser atingido com facilidade.
 
* Disparo premeditado de acordo com dados obtidos pelo radar.

## Ponto negativo 

* Quando o inimigo é localizado realizo o disparo assumindo o risco que o robô inimigo pode mudar rota antes de chegar ao destino que meu robô projeta o disparo.


> O robô foi desenvolvido e testado contra os robôs existentes do robocode, obtendo bons resultdos.

## Aprendizado ao desenvolver meu robô 

* Utilizando a linguagem C#, tive uma melhor segurança de tipo e desempenho em desenvolvimento.

